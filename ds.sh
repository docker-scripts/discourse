#!/bin/bash

launcher=discourse_docker/launcher

cmd_start() {
    $launcher start $CONTAINER
    $launcher start $CONTAINER-mail-receiver
}

cmd_stop() {
    $launcher stop $CONTAINER-mail-receiver
    $launcher stop $CONTAINER
}

cmd_restart() {
    $launcher restart $CONTAINER
    $launcher restart $CONTAINER-mail-receiver
}

cmd_shell() {
    $launcher enter $CONTAINER
}

cmd_remove() {
    $launcher destroy $CONTAINER-mail-receiver
    $launcher destroy $CONTAINER
    [[ -n $NETWORK ]] && docker network disconnect $NETWORK $CONTAINER 2>/dev/null
    ds revproxy rm
    rm -f /etc/cron.d/"$(echo $(basename $(pwd)) | tr . -)"-restart
    rm -f /etc/cron.d/"$(echo $(basename $(pwd)) | tr . -)"-copy-ssl-cert
}

cmd_make() {
    chmod +x .

    # add a revproxy domain and get a ssl-cert
    ds revproxy add
    ds revproxy ssl-cert
    ds copy-ssl-cert

    # copy the app and mail-receiver config files
    _copy_app_config_file
    _copy_mail_receiver_config_file

    # build the containers
    $launcher rebuild $CONTAINER
    $launcher rebuild $CONTAINER-mail-receiver

    # make some other configurations
    _create_cron_jobs
    ds exec rails runner "SiteSetting.force_https = true"

}

_copy_app_config_file() {
    local yml_file=discourse_docker/containers/$CONTAINER.yml
    if [[ -f $yml_file ]]; then
        echo "Config file '$yml_file' already exists. Not touching it."
        return
    fi

    # copy config file
    cp discourse_docker/samples/standalone.yml $yml_file
    chmod 640 $yml_file

    # comment out the exposed ports 80 and 443, since the container
    # will get the http(s) requests from 'revproxy'
    sed -i $yml_file \
        -e '/:80/ s/^/#/' \
        -e '/:443/ s/^/#/'

    # connect the container to the network of docker-scripts and add an alias
    # so that it can be accessed from 'revproxy'
    cat <<EOF >> $yml_file

docker_args:
  - "--network $NETWORK"
  - "--network-alias $DOMAIN"
EOF

    # enable ssl
    sed -i $yml_file \
        -e '/web.ssl.template.yml/ s/#-/-/'

    # uncomment some params
    sed -i $yml_file \
        -e 's/#db_shared_buffers:/db_shared_buffers:/' \
        -e 's/#UNICORN_WORKERS:/UNICORN_WORKERS:/'

    # set hostname
    sed -i $yml_file \
        -e "s/DISCOURSE_HOSTNAME:.*/DISCOURSE_HOSTNAME: $DOMAIN/" \
        -e 's/#DOCKER_USE_HOSTNAME:/DOCKER_USE_HOSTNAME:/'

    # set admin email
    sed -i $yml_file \
        -e "s/.*DISCOURSE_DEVELOPER_EMAILS:.*/  DISCOURSE_DEVELOPER_EMAILS: $ADMIN_EMAIL/"

    # email settings
    sed -i $yml_file \
        -e "s/.*DISCOURSE_SMTP_ADDRESS:.*/  DISCOURSE_SMTP_ADDRESS: $SMTP_SERVER/" \
        -e "s/.*DISCOURSE_SMTP_PORT:.*/  DISCOURSE_SMTP_PORT: ${SMTP_PORT:-587}/" \
        -e "s/.*DISCOURSE_SMTP_USER_NAME:.*/  DISCOURSE_SMTP_USER_NAME: $SMTP_USER/" \
        -e "s/.*DISCOURSE_SMTP_PASSWORD:.*/  DISCOURSE_SMTP_PASSWORD: $SMTP_PASS/" \
        -e "s/.*DISCOURSE_SMTP_ENABLE_START_TLS:.*/  DISCOURSE_SMTP_ENABLE_START_TLS: ${SMTP_TLS:-true}/"

    # fix the base path of the mounted volumes
    sed -i $yml_file \
        -e "s,/var/discourse,$(pwd)/discourse_docker,"

    # add the plugins
    for plugin in $PLUGINS; do
        sed -i $yml_file \
            -e "/docker_manager\.git/ {p ; s,git clone .*,git clone $plugin,}"
    done
}

_copy_mail_receiver_config_file() {
    local yml_file=discourse_docker/containers/$CONTAINER-mail-receiver.yml
    if [[ -f $yml_file ]]; then
        echo "Config file '$yml_file' already exists. Not touching it."
        return
    fi

    # copy config file
    cp discourse_docker/samples/mail-receiver.yml $yml_file
    chmod 640 $yml_file

    # listen to the port 2501 on the host,
    # instead of the port 25, which is probably being used
    sed -i $yml_file \
        -e 's/25:25/2501:25/'

    # customize MAIL_DOMAIN and DISCOURSE_BASE_URL
    sed -i $yml_file \
        -e "s/MAIL_DOMAIN:.*/MAIL_DOMAIN: $DOMAIN/" \
        -e "s,DISCOURSE_BASE_URL:.*,DISCOURSE_BASE_URL: 'https://$DOMAIN',"

    # fix the base path of the mounted volumes
    sed -i $yml_file \
        -e "s,/var/discourse,$(pwd)/discourse_docker,"
}

_create_cron_jobs() {
    local dir=$(basename $(pwd))
    local name=$(echo $dir | tr . -)
    mkdir -p /etc/cron.d

    # create a cron job that restarts the application once a week
    cat <<EOF > /etc/cron.d/${name}-restart
# restart the application @$dir each week
0 0 * * 0  root  bash -l -c "ds @$dir restart &> /dev/null"
EOF

    # create a cron job that copies the ssl cert from revproxy once a week
    cat <<EOF > /etc/cron.d/${name}-copy-ssl-cert
# copy the ssl cert @$dir each week
0 0 * * 0  root  bash -l -c "ds @$dir copy-ssl-cert &> /dev/null"
EOF
}
