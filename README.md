# Discourse in a container

If you want to install [Discourse](https://www.discourse.org) with
Docker in a dedicated VPS, you should better follow these
instructions:
https://github.com/discourse/discourse/blob/master/docs/INSTALL-cloud.md

If for some reason you need to install Discourse on the same VPS with
other docker-script applications, then follow the instructions
here.

Guide: https://docker-scripts.gitlab.io/apps/discourse.html

## Installation

  - First install `ds` and `revproxy`:
    + https://gitlab.com/docker-scripts/ds#installation
    + https://gitlab.com/docker-scripts/revproxy#installation

  - Make sure that you have also installed a simple SMTP server:
    + https://docker-scripts.gitlab.io/simple-smtp-server.html

  - Then get the scripts: `ds pull discourse`

  - Create a directory for the container: `ds init discourse @discourse.example.org`

  - Personalize the settings:
    `cd /var/ds/discourse.example.org/ ; vim settings.sh`

  - Build and start the container: `ds make`

  - Open https://discourse.example.org and continue the setup and
    configuration of the site. For more details look at:
    For more details look at:
    [Start Discourse](https://github.com/discourse/discourse/blob/main/docs/INSTALL-cloud.md#8-start-discourse).

## Backup and restore

```bash
ds backup
ds restore
ds discourse
```

## Administration and Maintenance

The docker-scripts for Discourse are actually just a shallow wrapper
to the official docker container. So you can also manage the container
with `launcher`, like this:

```bash
cd /var/ds/discourse.example.org/
cd discourse_docker/
./launcher
./launcher stop discourse.example.org
./launcher start discourse.example.org
./launcher enter discourse.example.org
./launcher logs discourse.example.org
```

In particular, you can change the settings on
`containers/discourse.example.org.yml` and then rebuild the container
with: `./launcher rebuild discourse.example.org`.

For more details look at:
- https://github.com/discourse/discourse/blob/master/docs/INSTALL-cloud.md#add-more-discourse-features
- https://github.com/discourse/discourse/blob/master/docs/ADMIN-QUICK-START-GUIDE.md
