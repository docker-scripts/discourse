APP=discourse

DOMAIN="discourse.example.org"
CONTAINER=$DOMAIN

ADMIN_EMAIL="admin@example.org"

### SMTP server used for sending notification emails.
### You can build an SMTP server as described here:
### https://docker-scripts.gitlab.io/simple-smtp-server.html
SMTP_SERVER=smtp.example.org
SMTP_PORT=25
SMTP_USER=
SMTP_PASS=
SMTP_TLS=true

### plugins to be installed
PLUGINS="
    https://github.com/discourse/discourse-solved
    https://github.com/discourse/discourse-calendar
    https://github.com/discourse/discourse-reactions
    https://github.com/discourse/discourse-chat-integration
    https://github.com/discourse/discourse-math
    https://github.com/discourse/discourse-subscriptions
    https://github.com/discourse/discourse-docs
    https://github.com/discourse/discourse-user-notes
    https://github.com/discourse/discourse-voting
    https://github.com/discourse/discourse-assign
    https://github.com/discourse/discourse-follow
    https://github.com/discourse/discourse-restricted-replies
    https://github.com/discourse/discourse-bcc
    https://github.com/discourse/discourse-graphviz
    https://github.com/discourse/discourse-push-notifications
    https://github.com/discourse/discourse-rss-polling
"
