cmd_clone_help() {
    cat <<_EOF
    clone <domain>
        Clone this discourse instance to a new domain.

_EOF
}

cmd_clone() {
    # get and check the domain
    local domain=$1
    test -z $domain && fail "Error: No domain given.\nUsage:\n$(cmd_clone_help)\n"
    [[ $domain = $DOMAIN ]] && fail "Error: '$domain' is the same as the current domain.\n"
    local dir=$CONTAINERS/$domain
    test -d $dir && fail "Error: Directory '$dir' seems to be already in use.\n"

    # make a backup
    set -x
    ds backup
    local backup_file=$(ls -t backup/*.tar.gz | head -1)

    # init the new instance
    ds init $APP @$domain
    rm $dir/settings.sh
    cp settings.sh $dir/
    sed -i $dir/settings.sh \
	-e "/^DOMAIN=/ c DOMAIN='$domain'"
    mkdir $dir/backup
    cp $backup_file $dir/backup/

    # build the new instance
    cd $dir
    ds make

    # restore the backup from the original site
    ds restore $backup_file

    # rename the old domain to the new one
    ds discourse remap $DOMAIN $domain
    ds exec rake posts:rebake

    # make sure that all outgoing emails are disabled
    ds exec rails runner "SiteSetting.disable_emails = 'non-staff'"
}
