cmd_backup_help() {
    cat <<_EOF
    backup
        Make a backup.

_EOF
}

cmd_backup() {
    ds discourse backup
    
    mkdir -p backup
    mv discourse_docker/shared/standalone/backups/default/* backup/
    ls -al --color backup/*
}
