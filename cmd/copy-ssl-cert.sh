# Copy ssl cert from revproxy/letsencrypt/
# to discourse_docker/shared/standalone/ssl/
cmd_copy-ssl-cert() {
    local ssl_cert_dir=$CONTAINERS/revproxy/letsencrypt/live/$DOMAIN
    local shared_ssl=discourse_docker/shared/standalone/ssl
    mkdir -p $shared_ssl
    cp -fL $ssl_cert_dir/cert.pem    $shared_ssl/ssl.crt
    cp -fL $ssl_cert_dir/privkey.pem $shared_ssl/ssl.key
}
