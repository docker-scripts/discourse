cmd_discourse_help() {
    cat <<_EOF
    discourse [...]
        Run discourse commands inside the container.

_EOF
}

cmd_discourse() {
    if test -t 0 ; then
        docker exec -it $CONTAINER env TERM=xterm discourse "$@"
    else
        docker exec -i $CONTAINER discourse "$@"
    fi
}
