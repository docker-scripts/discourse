cmd_upgrade_help() {
    cat <<_EOF
    upgrade
        Upgrade to the latest discourse and docker image.

_EOF
}

cmd_upgrade() {
    ds backup

    ### see: https://meta.discourse.org/t/how-do-i-manually-update-discourse-and-docker-image-to-latest/23325
    cd discourse_docker/
    git pull
    ./launcher rebuild $CONTAINER
    ./launcher rebuild $CONTAINER-mail-receiver
    ./launcher cleanup
}
