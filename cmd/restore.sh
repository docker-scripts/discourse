cmd_restore_help() {
    cat <<_EOF
    restore <backup-file.tar.gz>
        Restore from the given backup file.

_EOF
}

cmd_restore() {
    local file=$1
    [[ ! -f $file ]] && fail "Usage:\n$(cmd_restore_help)"
    [[ ${file%%.tar.gz} == $file ]] && fail "Usage:\n$(cmd_restore_help)"

    cp $file discourse_docker/shared/standalone/backups/default/
    ds discourse enable_restore
    ds discourse restore $(basename $file)

    # restore automatically disables emails
    # they need to be enabled after a restore
    ds exec rails runner "SiteSetting.disable_emails = 'no'"
}
